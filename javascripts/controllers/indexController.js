angular.module('app')
    .controller('indexController', ['$scope', '$http', 'dataFactory', function($scope, $http, dataFactory){

        $scope.seasonArray = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016];

        $scope.seasonSortedInfo = {};

        $scope.teamListAccordingToSeasons = {};

        $scope.teamMatchesAccordingToSeasons = {};

        $scope.matchData = [];

        $scope.displayPointsTable = false;

        $scope.displayAllMatchAnalysis = true;

        $scope.pointsObj = {};

        $scope.teaWinDetails = {};

        $scope.dataObj = {
            selected_season: $scope.seasonArray[0]
        };

        $scope.chartObj = {};

        $scope.winnerInfo = {};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: {lat: -34.397, lng: 150.644}
        });

        var geocoder = new google.maps.Geocoder();
        function geocodeAddress(geocoder, resultsMap) {
            var address = $scope.seasonSortedInfo[$scope.dataObj.selected_season][$scope.matchIndex].city;
            console.log(address);
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === 'OK') {
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location
                    });
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }

        $scope.getMatchInfo = function() {
            return dataFactory.getMatchData().then(function(response){
                $scope.matchData = response.data;
                console.log($scope.matchData);
                for (var i = 0 ; i < $scope.seasonArray.length ; i++) {
                    $scope.seasonSortedInfo[$scope.seasonArray[i]] = [];
                    $scope.teamListAccordingToSeasons[$scope.seasonArray[i]] = [];
                }

                for (var j = 0 ; j < $scope.matchData.length ; j++){
                    for (var key in $scope.seasonSortedInfo){
                        if (key == $scope.matchData[j].season) {
                            $scope.seasonSortedInfo[key].push($scope.matchData[j]);
                        }
                    }
                }
            });
        };

        $scope.getMatchInfo().then(function () {
            for (var key in $scope.seasonSortedInfo){
                angular.forEach($scope.seasonSortedInfo[key], function(info){
                    if ($scope.teamListAccordingToSeasons[key].indexOf(info.team1) == -1){
                        $scope.teamListAccordingToSeasons[key].push(info.team1);
                    }
                });
            }
            $scope.calculatePoints($scope.dataObj.selected_season);
            console.log($scope.teamListAccordingToSeasons);
        });

        $scope.clickTab = function(index) {
            $scope.index = index;
            if (index == 1) {
                $scope.displayPointsTable = false;
                $scope.displayAllMatchAnalysis = true;
            } else if (index == 2){
                $scope.displayPointsTable = true;
                $scope.displayAllMatchAnalysis = false;
            } else {
                $scope.displayPointsTable = false;
                $scope.displayAllMatchAnalysis = false;
            }
        };
        $scope.clickTab(1);

        $scope.getLocationOnMap = function(index) {
            $scope.matchIndex = index;
            geocodeAddress(geocoder, map);
        };

        $scope.calculatePoints = function(season) {
            $scope.winnerInfo = {};
            $scope.teaWinDetails = {};
            $scope.chartObj = {};
            $scope.chartObj[season] = {};

           angular.forEach($scope.seasonSortedInfo[season], function(item){
               $scope.teaWinDetails[item.winner] = [];
               $scope.winnerInfo[item.winner] = {};
               $scope.winnerInfo[item.winner].win = 0;
               $scope.winnerInfo[item.winner].lost = 0;
            });

            angular.forEach($scope.seasonSortedInfo[season], function(item){
                $scope.winnerInfo[item.winner].win += 1;
                $scope.winnerInfo[item.winner].points = 2 * $scope.winnerInfo[item.winner].win;
                if (item.winner === item.team1){
                    $scope.winnerInfo[item.team2].lost += 1;
                } else if (item.winner === item.team2) {
                    $scope.winnerInfo[item.team1].lost += 1;
                }
                $scope.teaWinDetails[item.winner].push(item);
            });
            for (var key in $scope.winnerInfo){
                $scope.winnerInfo[key].total_matches = $scope.winnerInfo[key].win + $scope.winnerInfo[key].lost;
            }

            console.log($scope.teaWinDetails);
            console.log($scope.chartObj);

            $scope.chartObj[season] = {
                chart: {
                    height: 500,
                    width: 500,
                    type: 'column'
                },
                plotOptions: {
                    series: {
                        stacking: ''
                    }
                },
                series: $scope.chartSeries,
                title: {
                    text: 'Hello'
                }
            }
        };



    }]);