/**
 * Created by keshavdhawan on 05/05/17.
 */
angular.module('app')
    .factory('dataFactory', ['$http', function($http) {

        var dataFactory = {};

        dataFactory.getMatchData = function() {
            return $http({
                method: 'GET',
                url: 'javascripts/data/matches.json'
            });
        };

        return dataFactory;

    }]);