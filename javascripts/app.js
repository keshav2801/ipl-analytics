angular
    .module('app', [
        'ui.router',
        'ui.materialize',
        'highcharts-ng'
    ])
    .config(config);

function config($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: '/templates/landing_page.html',
            controller: 'indexController'
        });

    $urlRouterProvider.otherwise('home');
}
